package inv.project.dao;

import inv.project.model.Curso;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CursoDaolmp implements CursoDao{

	@Autowired
	private SessionFactory sessionFactory;

	public Integer save(Curso curso) {
		sessionFactory.getCurrentSession().save(curso);
		return curso.getIdCurso();
	}

	public Curso get(Integer id) {
		return sessionFactory.getCurrentSession().get(Curso.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<Curso> list() {
		Session session = sessionFactory.getCurrentSession();
		Query<Curso> query = session.createQuery("from Curso");
		List<Curso> cursos = query.list();
			return cursos;
	}

	public void update(Integer id, Curso curso) {
		Session session = sessionFactory.getCurrentSession();
		Curso cursoActualizada = session.byId(Curso.class).load(id);
		cursoActualizada.setNombre(curso.getNombre());
		cursoActualizada.setProfesor(curso.getProfesor());
		cursoActualizada.setFecha(curso.getFecha());
		
		session.flush();
	}

	public void delete(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Curso curso = session.byId(Curso.class).load(id);
		session.delete(curso);
	}

}
