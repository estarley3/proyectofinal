package inv.project.dao;


import inv.project.model.Matricula;


import java.util.List;

public interface MatriculaDao {
	
	Integer save(Matricula matricula);

	Matricula get(Integer id);

	List<Matricula> list();

	void update(Integer id, Matricula matricula);

	void delete(Integer id);

}
