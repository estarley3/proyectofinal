package inv.project.dao;

import java.util.List;


import inv.project.model.Matricula;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class MatriculaDaoImp implements MatriculaDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	public Integer save(Matricula matricula) {
		sessionFactory.getCurrentSession().save(matricula);
		return matricula.getIdMatricula();
	}

	public Matricula get(Integer id) {
		return sessionFactory.getCurrentSession().get(Matricula.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<Matricula> list() {
		Session session = sessionFactory.getCurrentSession();
		Query<Matricula> query = session.createQuery("from Matricula");
		List<Matricula> matriculas = query.list();
			return matriculas;
	}

	public void update(Integer id, Matricula matricula) {
		Session session = sessionFactory.getCurrentSession();
		Matricula matriculaActualizada = session.byId(Matricula.class).load(id);
		matriculaActualizada.setIdMatricula(matricula.getIdMatricula());
		matriculaActualizada.setFechamatricula(matricula.getFechamatricula());
		session.flush();
		
	}

	public void delete(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Matricula matricula = session.byId(Matricula.class).load(id);
		session.delete(matricula);
		
	}

}
