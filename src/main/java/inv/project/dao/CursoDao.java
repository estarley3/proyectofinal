package inv.project.dao;

import inv.project.model.Curso;


import java.util.List;


public interface CursoDao {
	Integer save(Curso curso);

	Curso get(Integer id);

	List<Curso> list();

	void update(Integer id, Curso curso);

	void delete(Integer id);
}
