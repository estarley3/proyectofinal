package inv.project.model;

import javax.persistence.*;

@Entity(name = "Matricula")
public class Matricula {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idMatricula;
	private String fechamatricula;
	
	@OneToOne
	private Persona persona;
	
	@OneToOne
	private Curso curso;
	
	public int getIdMatricula() {
		return idMatricula;
	}
	public void setIdMatricula(int idMatricula) {
		this.idMatricula = idMatricula;
	}
	public Persona getPersona() {
		return persona;
	}
	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	public Curso getCurso() {
		return curso;
	}
	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	public String getFechamatricula() {
		return fechamatricula;
	}
	public void setFechamatricula(String fechamatricula) {
		this.fechamatricula = fechamatricula;
	}

}
