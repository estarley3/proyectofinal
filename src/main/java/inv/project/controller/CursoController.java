package inv.project.controller;

import inv.project.model.Curso;
import inv.project.service.CursoService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class CursoController {

	@Autowired
	private CursoService cursoService;

	//Agregar nueva persona
	@PostMapping("/curso")
	public ResponseEntity<?> save(@RequestBody Curso curso) {
		int id = cursoService.save(curso);
		return ResponseEntity.ok()
				.body("New curso has been saved with ID:" + id);
	}

	//Obtener persona por id
	@GetMapping("/curso/{id}")
	public ResponseEntity<Curso> get(@PathVariable("id") int id) {
		Curso curso = cursoService.get(id);
		return ResponseEntity.ok().body(curso);
	}
	
	@GetMapping("/cursos")
	public ResponseEntity<List<Curso>> get() {
		List<Curso> cursos = cursoService.list();
		return ResponseEntity.ok().body(cursos);
	}
	
	  @PutMapping("/curso/{id}")
	   public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody Curso curso) {
	     cursoService.update(id, curso);
	      return ResponseEntity.ok().body("Curso has been updated successfully.");
	   }
	  
	  
	   @DeleteMapping("/curso/{id}")
	   public ResponseEntity<?> delete(@PathVariable("id") int id) {
	      cursoService.delete(id);
	      return ResponseEntity.ok().body("Curso has been deleted successfully.");
	   }
}
