package inv.project.controller;

import inv.project.model.Matricula;
import inv.project.service.MatriculaService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class MatriculaController {
	@Autowired
	private MatriculaService matriculaService;

	//Agregar nueva persona
	@PostMapping("/matricula")
	public ResponseEntity<?> save(@RequestBody Matricula matricula) {
		int id = matriculaService.save(matricula);
		return ResponseEntity.ok()
				.body("New matricula has been saved with ID:" + id);
	}

	//Obtener persona por id
	@GetMapping("/matricula/{id}")
	public ResponseEntity<Matricula> get(@PathVariable("id") int id) {
		Matricula matricula = matriculaService.get(id);
		return ResponseEntity.ok().body(matricula);
	}
	
	@GetMapping("/matriculas")
	public ResponseEntity<List<Matricula>> get() {
		List<Matricula> matriculas = matriculaService.list();
		return ResponseEntity.ok().body(matriculas);
	}
	
	  @PutMapping("/matricula/{id}")
	   public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody Matricula matricula) {
	     matriculaService.update(id, matricula);
	      return ResponseEntity.ok().body("Matricula has been updated successfully.");
	   }
	  
	  
	   @DeleteMapping("/matricula/{id}")
	   public ResponseEntity<?> delete(@PathVariable("id") int id) {
	      matriculaService.delete(id);
	      return ResponseEntity.ok().body("Matricula has been deleted successfully.");
	   }
	
	
	
}
