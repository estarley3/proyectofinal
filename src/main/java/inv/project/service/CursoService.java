package inv.project.service;

import java.util.List;

import inv.project.model.Curso;

public interface CursoService {
	
	Integer save(Curso curso);

	Curso get(Integer id);

	List<Curso> list();

	void update(Integer id, Curso curso);

	void delete(Integer id);
}
