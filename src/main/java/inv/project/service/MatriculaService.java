package inv.project.service;

import java.util.List;

import inv.project.model.Matricula;

public interface MatriculaService {
	
	Integer save(Matricula matricula);

	Matricula get(Integer id);

	List<Matricula> list();

	void update(Integer id, Matricula matricula);

	void delete(Integer id);
}
