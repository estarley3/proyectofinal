package inv.project.service;

import inv.project.dao.MatriculaDao;

import inv.project.model.Matricula;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class MatriculaServicelmpl implements MatriculaService{

	@Autowired
	private MatriculaDao matriculaDao;

	@Transactional
	public Integer save(Matricula matricula) {
		return matriculaDao.save(matricula);
	}
	
	public Matricula get(Integer id) {
		return matriculaDao.get(id);
	}

	public List<Matricula> list() {
		return matriculaDao.list();
	}
	
	@Transactional
	public void update(Integer id, Matricula matricula) {
       matriculaDao.update(id, matricula);

	}
	@Transactional
	public void delete(Integer id) {
		matriculaDao.delete(id);

	}
	
	
}
