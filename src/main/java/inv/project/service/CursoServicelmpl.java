package inv.project.service;

import inv.project.dao.CursoDao;

import inv.project.model.Curso;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class CursoServicelmpl implements CursoService {
	
	@Autowired
	private CursoDao cursoDao;

	@Transactional
	public Integer save(Curso curso) {
		return cursoDao.save(curso);
	}
	@Transactional
	public Curso get(Integer id) {
		return cursoDao.get(id);
	}
	@Transactional
	public List<Curso> list() {
		return cursoDao.list();
		
	}
	
	@Transactional
	public void update(Integer id, Curso curso) {
       cursoDao.update(id, curso);

	}
	@Transactional
	public void delete(Integer id) {
		cursoDao.delete(id);

	}
}
